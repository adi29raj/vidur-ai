import { Configuration, OpenAIApi } from "openai";
import express from "express";
import "dotenv/config.js";
// import MicRecorder from "mic-recorder-to-mp3";
// import path from "path";
// import pkg from "node-lame";
// const { Lame } = pkg;
// import record from "node-record-lpcm16";
// import ffmpeg from "fluent-ffmpeg";
// import { SpeechRecorder, devices } from "speech-recorder";
// import TextToSpeech from "text-to-speech-js";
// import textToSpeech from "@google-cloud/text-to-speech";
// import players from "play-sound";
// import sound from "sound-play";

import fs from "fs";
import ffmpeg from "fluent-ffmpeg";
import mic from "mic";
import pkg from "stream";
const { Readable } = pkg;
import { path } from "@ffmpeg-installer/ffmpeg";
import gTTS from "gtts";
import { playAudioFile } from "audic";

const ffmpegPath = path;

ffmpeg.setFfmpegPath(ffmpegPath);

const port = process.env.PORT || 7777;

const app = express();

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

export const sendResponse = (res, message, statusCode) => {
  // res.status(statusCode).json({
  //     error: {
  //       message
  //     },
  // })
  console.log(statusCode, message);
  return {
    statusCode,
    message,
  };
};

export default async function getResFromOpenAI(req = {}, res = {}) {
  if (!configuration.apiKey) {
    sendResponse(
      res,
      "OpenAI API key not configured, please follow instructions in README.md",
      500
    );
    return;
  }

  const userMessage = req.body.userMessage;

  try {
    const completion = await openai.createChatCompletion({
      model: "gpt-3.5-turbo", // to:do change it gpt-4
      messages: generatePrompt(userMessage),
      // temperature: 0.6,
    });
    return sendResponse(res, completion.data.choices[0].message.content, 200);
  } catch (error) {
    // Consider adjusting the error handling logic for your use case
    if (error.response) {
      console.error(error.response.status, error.response.data);
      return sendResponse(res, error.response.data, error.response.status);
    } else {
      console.error(`Error with OpenAI API request: ${error.message}`);
      return sendResponse(res, "An error occurred during your request.", 500);
    }
  }
}

function generatePrompt(userMessage) {
  const messages = [
    {
      role: "system",
      content:
        "You are an AI assistant like JARVIS from Iron Man movies. Add some sarcastic or intelligent quips and onel-liners with answers if fits. Also add 'Sure boss or On it boss' wherever suits.Also as I am calling a lot of api's your responses are always delayed so always apologize for that upfront",
    },
    {
      role: "user",
      content: userMessage,
    },
  ];
  return messages;
}

function recordAudio(filename) {
  return new Promise((resolve, reject) => {
    const micInstance = mic({
      rate: "16000",
      channels: "1",
      fileType: "wav",
    });

    const micInputStream = micInstance.getAudioStream();
    const output = fs.createWriteStream(filename);
    const writable = new Readable().wrap(micInputStream);

    // console.log("Recording... Press Ctrl+C to stop.");

    writable.pipe(output);

    micInstance.start();

    setTimeout(() => {
      micInstance.stop();
      console.log("Finished recording");
      resolve();
    }, 8000);

    // process.on("SIGINT", () => {
    //   micInstance.stop();
    //   console.log("Finished recording");
    //   resolve();
    // });

    micInputStream.on("error", (err) => {
      reject(err);
    });
  });
}

async function text2Speech(speech, resultAudioFilename) {
  // let speech = "Welcome to GeeksforGeeks";
  const gtts = new gTTS(speech, "en");

  await gtts.save(resultAudioFilename, function (err, result) {
    if (err) {
      throw new Error(err);
    }
    console.log("Text to speech converted!");
  });
}

async function main() {
  try {
    const audioFilename = "recorded_audio.wav";
    const resultAudioFilename = "result_audio.mp3";
    console.log("Listening...");
    await recordAudio(audioFilename);
    console.log("Processing audio...");
    const transcript = await openai.createTranscription(
      fs.createReadStream(audioFilename),
      "whisper-1"
    );
    console.log("Query  : ", transcript.data.text);
    const request = {
      body: {
        userMessage: transcript.data.text,
      },
    };
    console.log("Processing query...");
    const response = await getResFromOpenAI(request);
    if (response?.statusCode == 200) {
      await text2Speech(response.message, resultAudioFilename);
      await playAudioFile(resultAudioFilename);
      console.log("Output played!!");
      fs.unlinkSync(audioFilename);
      fs.unlinkSync(resultAudioFilename);
      // main();
    }
  } catch (e) {
    console.log(e);
  }
}

main();

app.listen(port, () => console.log(port));
